package org.bitbucket.kevstigneev.helloconsul

import spock.lang.*
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import java.util.concurrent.TimeUnit

@Slf4j
@Stepwise
class ConsulTest extends Specification {

    def consulUrl = new URI(System.getProperty('consul') + '/').normalize().toURL()

    @Timeout(value = 10, unit = TimeUnit.SECONDS)
    def "Find Consul cluster and its configuration"() {
        log.info "Start 'Find Consul cluster and its configuration'"

        given: "Consul API HTTP(S) endpoint URL"
            def consulApiUrl = new URL(consulUrl, 'v1/')
            log.debug "consulApiUrl = ${consulApiUrl}"
        when: "I query Consul REST API for the cluster nodes"
            def peersText = new URL(consulApiUrl, "status/peers").text
            def peers = new JsonSlurper().parseText(peersText)
            log.info "Consul peers are ${peers}"
        then: "I get a list of 3 nodes"
            peers instanceof List
            peers.size() == 3
        when: "I query Consul REST API for the cluster leader"
            def leaderText = new URL(consulApiUrl, "status/leader").text
            def leader = new JsonSlurper().parseText(leaderText)
            log.info "Consul cluster leader = ${leader}"
        then: "just one of them is a leader"
            leader in peers
        when: "I query each cluster node for the leader"
            def leaders = peers.collect {
                def host = "//${it}".toURI().host
                def nodeUrl = new URL(consulUrl.protocol, host, consulUrl.port, "/v1/")
                new URL(nodeUrl, "status/leader").text
            }
            log.info "Nodes think that cluster leader is ${leaders}"
        then: "they all agree on it"
            (leaders as Set).size() == 1
    }

    @Timeout(value = 10, unit = TimeUnit.SECONDS)
    def "Consul nodes can be discovered via Consul itself"() {
        log.info "Start 'Consul nodes can be discovered via Consul itself'"
        def service = "consul"

        given: "Consul API HTTP(S) endpoint URL"
            def consulApiUrl = new URL(consulUrl, 'v1/')
            log.debug "consulApiUrl = ${consulApiUrl}"
        when: "I query Consul for 'consul' service nodes"
            def nodesText = new URL(consulApiUrl, "catalog/service/${service}").text
            def allJson = new JsonSlurper().parseText(nodesText)
        then: "I get a list of nodes"
            def allNodes = allJson.collect {
                (it.ServiceAddress ?: it.Address) + ':' + it.ServicePort
            }
            log.info "From the Consul catalog '${service}' nodes are ${allNodes}"
        when: "I query Consul 'consul' service nodes that are healthy"
            def healthyText = new URL(consulApiUrl, "health/service/${service}?passing").text
            def healthyJson = new JsonSlurper().parseText(healthyText)
        then: "I get the same list of nodes"
            def healthyNodes = healthyJson.collect {
                (it.Service.Address ?: it.Node.Address) + ':' + it.Service.Port
            }
            log.info "From the Consul catalog healthy '${service}' nodes are ${allNodes}"
            allNodes.size() == healthyNodes.size()
            (allNodes as Set) == (healthyNodes as Set)
        when: "I query Consul REST API for the cluster nodes"
            def peersText = new URL(consulApiUrl, "status/peers").text
            def peers = new JsonSlurper().parseText(peersText)
             log.info "Consul peers are ${peers}"
       then: "I get the same list of nodes"
            allNodes.size() == peers.size()
            (allNodes as Set) == (peers as Set)
    }

}
