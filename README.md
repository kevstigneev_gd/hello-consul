Consul instance testing primer
==============================

Prerequisites:

- Maven 3.
- JDK 8.

Run tests:

    mvn test -Dconsul=<CONSUL_URL>

Where

- `<CONSUL_URL>` - an HTTP(S) endpoint for the Consul cluster, usually the load balancer.

HTML report is in `target/spock-reports`, JUnit ones - in `target/surefire-reports`.


Logging
-------

Tests use SLF4J for logging. To control logging verbosity set
`org.slf4j.simpleLogger.defaultLogLevel` system property in Maven command line
to the desired logging level (one of TRACE, DEBUG, INFO, WARN ERROR, FATAL).
E.g. `-Dorg.slf4j.simpleLogger.defaultLogLevel=DEBUG`.


Run tests against a cluster behind a firewall
---------------------------------------------

It requires a host behind the firewall with SSH access to it (`<SSH_JUMP_HOST>`).

Run SOCKS proxy:

    ssh -vND 1080 <SSH_JUMP_HOST>

Then add `-DsocksProxyHost=localhost` to Maven command line above. See
https://docs.oracle.com/javase/8/docs/api/java/net/doc-files/net-properties.html#Proxies
for further details.
